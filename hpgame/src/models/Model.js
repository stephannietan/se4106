
class Model{

  constructor (doc = {}){
    Object.assign(this, doc)
  }

}

export default Model
