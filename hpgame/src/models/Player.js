import Model from './Model'
import Team from './Team'

class Player extends Model {

  getFullName() {
    return this.firstName + " " + this.lastName
  }

}

export default Player
