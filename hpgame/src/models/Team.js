import Model from './Model'
import Player from './Player'

class Team extends Model {


  addPlayer(player) {
    player.team = this
    this.players.push(player)
  }
}


export default Team
